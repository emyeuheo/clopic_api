<?php

class ApiActionController extends BaseSessionController {

    public function getLike() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
            
            $like = Like::doLike($this->currentSession->user_id, $photoId);
            
            // Generate notification
            if(!empty($like)) {
                $photo = Photo::photoDetail($photoId);                
                $currentUser = User::getInfo($this->currentSession->user_id);
                $message = "{$currentUser->username} liked your photo";
                Notification::createNew($photo->user_id, $this->currentSession->user_id, 'like', $message, $photoId);
            }
            
            return $like;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postLike() {
        return $this->getLike();
    }
    
    public function getUnlike() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
            
            $like = Like::doUnlike($this->currentSession->user_id, $photoId);
            
            // Delete notification
            if(!empty($like)) {
                $photo = Photo::photoDetail($photoId);
                Notification::deleteByInfo($photo->user_id, $this->currentSession->user_id, 'like', $photoId);
            }
            
            return $like;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUnlike() {
        return $this->getUnlike();
    }
    
    public function getComment() {
        try {
            list($photoId, $text) = ClopicValidator::instance()->inputs(array('photo_id', 'text'));
            
            $comment = Comment::doComment($this->currentSession->user_id, $photoId, $text);
            
            // Generate notification
            if(!empty($comment)) {
                $photo = Photo::photoDetail($photoId);                
                $currentUser = User::getInfo($this->currentSession->user_id);
                $cropText = mb_substr($text, 0, 20, "UTF-8");
                $message = "{$currentUser->username} commented on your photo : \"$cropText\"";
                Notification::createNew($photo->user_id, $this->currentSession->user_id, 'comment', $message, $photoId);
            }
            
            return $comment;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postComment() {
        return $this->getComment();
    }
    
    public function getDeletecomment() {
        try {
            list($commentId) = ClopicValidator::instance()->inputs(array('comment_id'));
            
            $comment = Comment::deleteComment($this->currentSession->user_id, $commentId);
            
            // Delete notification
            if(!empty($comment)) {
                $photo = Photo::photoDetail($comment->photo_id);
                Notification::deleteByInfo($photo->user_id, $this->currentSession->user_id, 'comment', $comment->photo_id);
            }
            
            return $comment;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postDeletecomment() {
        return $this->getDeletecomment();
    }
    
    public function getNotification() {
        try {
            return Notification::getNotiFeed($this->currentSession->user_id);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postNotification() {
        return $this->getNotification();
    }
    
    public function getIsliked() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
            
            $like = Like::where('user_id', $this->currentSession->user_id)->where('photo_id', $photoId)->first();
            if(empty($like)) {
                return array(0);
            }
            
            return array(1);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postIsliked() {
        return $this->getIsliked();
    }
}
