<?php

class BaseSessionController extends BaseController {

    protected $currentSession;

    public function __construct() {
        try {
            $this->currentSession = LoginSession::current();
        } catch (Exception $ex) {
            App::abort($ex->getCode());
        }
    }

}
