<?php

class ApiPhotoController extends BaseSessionController {

    public function getUpload() {
        return array('error' => array('message' => 'Please use POST method'));
    }

    public function postUpload() {
        $validator = ClopicValidator::instance();
        $caption = Input::get('caption');
        $address = Input::get('address');
        $hashtagRaw = Input::get('hashtag');
        
        try {
            list($photoFile) = $validator->files(array('photo'));
            list($x, $y) = $validator->getXY();
            $validator->image($photoFile);
            $newPhoto = Photo::newPhoto($photoFile->getRealPath(), $this->currentSession->user_id, $caption);
            $newHashTags = Hashtag::newHashTags(Utils::explodeHashtag($hashtagRaw));
            MapHashPhoto::newMaps($newHashTags, $newPhoto->id);
            $location = PhotoLocation::setLocationForPhoto($newPhoto->id, $x, $y, $address);
            if ($location) {
                $location->unpack();
                Utils::copyProperty($newPhoto, $location, array('longitude', 'latitude', 'address'));
            }
            $newPhoto->asset();
        } catch (Exception $ex) {
            $newPhoto = Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }

        
        return $newPhoto;
    }

    public function getMyphoto() {
        try {
            $page = ClopicValidator::instance()->page();
            return Photo::getPhoto($this->currentSession->user_id, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postMyphoto() {
        return $this->getMyphoto();
    }

    public function getHashtag($mode = 'data') {
        $hashtag = '';
        if (Input::has('hashtag')) {
            $hashtag = Input::get('hashtag');
        }

        $page = 1;
        $userId = null;
        if (Input::has('page') && is_numeric(Input::get('page')) && Input::get('page') > 0) {
            $page = Input::get('page');
        }
        if(Input::has('user_id')) {
            $userId = Input::get('user_id');
        }
        
        try {
            return Photo::getPhotoByHashtag($hashtag, $page, $userId, $mode);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postHashtag($mode = 'data') {
        return $this->getHashtag($mode);
    }

    public function getDetail() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));

            return Photo::photoDetail($photoId);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postDetail() {
        return $this->getDetail();
    }

    public function getGetcomment() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
            $page = ClopicValidator::instance()->page();
            
            return Comment::getPhotoComment($photoId, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postGetcomment() {
        return $this->getGetcomment();
    }

    public function getFeed() {
        try {
            $page = ClopicValidator::instance()->page();
            
            $userIds = Utils::objColToArray(UserRelationship::getFollowing($this->currentSession->user_id), 'other_user_id');
            $userIds[] = $this->currentSession->user_id;

            return Photo::getPhoto($userIds, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postFeed() {
        return $this->getFeed();
    }

    public function getTest() {
        return PhotoLocation::setLocationForPhoto(2, 100, 100, 'Somewhere');
    }

    public function postTest() {
        return $this->getTest();
    }

    public function getUser() {
        try {
            list($userId) = ClopicValidator::instance()->inputOrDefault(array('user_id' => $this->currentSession->user_id));
            list($mustHasLocation) = ClopicValidator::instance()->inputOrDefault(array('has_location' => false));
            $page = ClopicValidator::instance()->page();
            return Photo::getPhoto($userId, $page, $mustHasLocation, 50);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUser() {
        return $this->getUser();
    }
    
    public function getSearcharound() {
        try {
            list($photoId, $distance) = ClopicValidator::instance()->numeric(array('photo_id', 'distance'));
            $page = ClopicValidator::instance()->page();
            return PhotoLocation::nearestByKm($photoId, $distance, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postSearcharound() {
        return $this->getSearcharound();
    }
    
    public function getTheclock() {
        try {
            list($startTime, $endTime) = ClopicValidator::instance()->inputs(array('start_time', 'end_time'));
            list($currentLong, $currentLat, $distance) = ClopicValidator::instance()->numeric(array('current_long', 'current_lat', 'distance'));
            $page = ClopicValidator::instance()->page();
            
            return Photo::theClock($this->currentSession->user_id, $startTime, $endTime, $currentLong, $currentLat, $distance, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postTheclock() {
        return $this->getTheclock();
    }
    
    public function getCollect($mode = NULL) {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
                        
            if($mode == 'check') {
                return array(Photo::where('original_id', $photoId)->where('user_id', $this->currentSession->user_id)->count());
            }
            
            return Photo::collectPhoto($this->currentSession->user_id, $photoId);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postCollect($mode = NULL) {
        return $this->getCollect($mode);
    }
    
    public function getDeletephoto() {
        try {
            list($photoId) = ClopicValidator::instance()->inputs(array('photo_id'));
            
            $return = Photo::deletePhoto($this->currentSession->user_id, $photoId);
            
            if(empty($return)) {
                return array(0);
            }
            
            return array(1);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postDeletephoto() {
        return $this->getDeletephoto();
    }
    
    public function getUncollect() {
        try {
            list($originalId) = ClopicValidator::instance()->inputs(array('photo_id'));
            
            $return = Photo::uncollectPhoto($this->currentSession->user_id, $originalId);
            
            if(empty($return)) {
                return array(0);
            }
            
            return array(1);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUncollect() {
        return $this->getUncollect();
    }
}
