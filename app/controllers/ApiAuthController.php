<?php

class ApiAuthController extends BaseController {

    public function getRegister() {
        if(!Input::has('email','username','password')) {
            return Error::getInstance()->getError(REGISTER_INVALID_FORM);
        }
        
        // Init
        $validator = new ClopicValidator();
        $username = Input::get('username');
        $password = Input::get('password');
        $email = Input::get('email');
        
        try {
            $validator->username($username);
            $validator->email($email);
            $validator->password($password);
            $user = User::newUser($username, $password, $email);
            $session = LoginSession::newSession($user);
            $user->session_key = $session->session_key;
            return $user;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postRegister() {
        return $this->getRegister();
    }
    
    public function getLogin() {
        if(!Input::has('username','password')) {
            return Error::getInstance()->getError(LOGIN_INVALID_FORM);
        }
        
        // Init
        $validator = new ClopicValidator();
        $username = Input::get('username');
        $password = Input::get('password');
        
        try {
            $user = $validator->login($username, $password);
            $session = LoginSession::newSession($user);
            return $session;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postLogin() {
        return $this->getLogin();
    }
    
    public function getTest() {
        return Request::ip();
    }
}