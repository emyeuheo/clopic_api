<?php

class ApiUserController extends BaseSessionController {

    public function getInfo() {
        if (Input::has('user_id')) {
            $userId = Input::get('user_id');
        } else {
            $userId = $this->currentSession->user_id;
        }
        return User::getInfo($userId);
    }

    public function postInfo() {
        return $this->getInfo();
    }

    public function getFollow() {
        if (!Input::has('user_id') && !Input::has('username')) {
            return Error::getInstance()->getError(FOLLOW_MISSING_INFO);
        }

        try {
            $user = User::where('id', Input::get('user_id'))->orWhere('username', Input::get('username'))->first();
            if (empty($user)) {
                return array();
            }

            $userFollowed = UserRelationship::isExist($this->currentSession->user_id, $user->id, UserRelationship::FOLLOWING);

            if (!empty($userFollowed)) {
                return $userFollowed;
            }

            return UserRelationship::createNew($this->currentSession->user_id, $user->id, UserRelationship::FOLLOWING);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postFollow() {
        return $this->getFollow();
    }

    public function getUnfollow() {
        if (!Input::has('user_id') && !Input::has('username')) {
            return Error::getInstance()->getError(FOLLOW_MISSING_INFO);
        }

        try {
            $user = User::where('id', Input::get('user_id'))->orWhere('username', Input::get('username'))->first();
            if (empty($user)) {
                return array();
            }
            
            return UserRelationship::remove($this->currentSession->user_id, $user->id, UserRelationship::FOLLOWING);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postUnfollow() {
        return $this->getUnfollow();
    }

    public function getUpdateinfo() {
        try {
            $dataUpdate = array();
            $dataUpdate['first_name'] = ((Input::get('first_name')) != NULL) ? Input::get('first_name') : NULL;
            $dataUpdate['last_name'] = ((Input::get('last_name')) != NULL) ? Input::get('last_name') : NULL;
            $dataUpdate['email'] = ((Input::get('email')) != NULL) ? Input::get('email') : NULL;
            $dataUpdate['description'] = ((Input::get('description')) != NULL) ? Input::get('description') : NULL;
            $dataUpdate['mobile_phone'] = ((Input::get('mobile_phone')) != NULL) ? Input::get('mobile_phone') : NULL;
            $dataUpdate['gender'] = ((Input::get('gender')) != NULL) ? Input::get('gender') : NULL;
            
            return User::updateInfo($this->currentSession->user_id, $dataUpdate);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }

    public function postUpdateinfo() {
        return $this->getUpdateinfo();
    }
    
    public function getPasswd() {
        try {
            list($oldPasswd, $newPasswd, $newPasswd2) = ClopicValidator::instance()->inputs(array('oldpasswd', 'newpasswd', 'newpasswd2'));
            
            return User::changePassword($this->currentSession->user_id, $oldPasswd, $newPasswd, $newPasswd2);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postPasswd() {
        return $this->getPasswd();
    }
    
    public function getUploadavatar() {
        try {
            list($avatar) = ClopicValidator::instance()->files(array('photo'));
            ClopicValidator::instance()->image($avatar);
            
            return User::uploadAvatar($this->currentSession->user_id, $avatar->getRealPath(), 'avatar');
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUploadavatar() {
        return $this->getUploadavatar();
    }

    public function getUploadcover() {
        try {
            list($avatar) = ClopicValidator::instance()->files(array('photo'));
            ClopicValidator::instance()->image($avatar);
            
            return User::uploadAvatar($this->currentSession->user_id, $avatar->getRealPath(), 'cover');
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUploadcover() {
        return $this->getUploadcover();
    }
    
    public function getHashtag() {
        try {
            list($userId, $page) = ClopicValidator::instance()->inputOrDefault(array(
                'user_id' => $this->currentSession->user_id,
                'page'    => 1
            ));
            
            return Hashtag::getUserHashtag($userId, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postHashtag() {
        return $this->getHashtag();
    }
    
    public function getIsfollowing() {
        if (!Input::has('user_id') && !Input::has('username')) {
            return Error::getInstance()->getError(FOLLOW_MISSING_INFO);
        }

        try {
            $user = User::where('id', Input::get('user_id'))->orWhere('username', Input::get('username'))->first();
            if (empty($user)) {
                return array(0);
            }

            $userFollowed = UserRelationship::isExist($this->currentSession->user_id, $user->id, UserRelationship::FOLLOWING);

            if (!empty($userFollowed)) {
                return array(1);
            } else {
                return array(0);
            }

        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postIsfollowing() {
        return $this->getIsfollowing();
    }
    
    public function getFollower() {
        try {
            $page = ClopicValidator::instance()->page();
            list($userId) = ClopicValidator::instance()->inputOrDefault(array('user_id' => $this->currentSession->user_id));
            
            return UserRelationship::getFollowerUsers($userId, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postFollower() {
        return $this->getFollower();
    }
    
    public function getFollowing() {
        try {
            $page = ClopicValidator::instance()->page();
            list($userId) = ClopicValidator::instance()->inputOrDefault(array('user_id' => $this->currentSession->user_id));
            
            return UserRelationship::getFollowingUsers($userId, $page);
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postFollowing() {
        return $this->getFollowing();
    }
    
    // maybe remove this in future
    public function getFollow2d() {
        try {
            $page = ClopicValidator::instance()->page();
            list($userId) = ClopicValidator::instance()->inputOrDefault(array('user_id' => $this->currentSession->user_id));
            
            Paginator::setCurrentPage($page);
            
            $rels = UserRelationship::where('user_id', $userId)
                    ->orWhere('other_user_id', $userId)
                    ->simplePaginate(Photo::MAX_ITEM_A_PAGE)
                    ->all();
            
            if(empty($rels)) {
                return array();
            }

            $userIds = array();

            foreach($rels as $rel) {
                if($rel->user_id == $userId) {
                    $userIds[] = $rel->other_user_id;
                } else {
                    $userIds[] = $rel->user_id;
                }
            }

            $users = User::whereRaw('id IN ('.implode(',', $userIds).')')->orderBy('username','ASC')->get();
            foreach($users as $user) {
                $user->asset(true);
            }
            
            return $users;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postFollow2d() {
        return $this->getFollow2d();
    }
    ///
}
