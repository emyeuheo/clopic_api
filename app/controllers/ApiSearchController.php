<?php

class ApiSearchController extends BaseSessionController {
        
    public function getHashtag() {
        try {
            list($keyword) = ClopicValidator::instance()->inputs(array('keyword'));
            list($userId, $page) = ClopicValidator::instance()->inputOrDefault(array(
                'user_id' => null,
                'page' => 1
            ));
            
            Paginator::setCurrentPage($page);
            
            $foundHashtags = Hashtag::whereRaw("hash_tag LIKE '$keyword%'")->simplePaginate(20)->all();
            foreach($foundHashtags as $hashtagObj) {
                $hashtagObj->total_photo = MapHashPhoto::where('hash_tag_id', $hashtagObj->id)->count();
            }
            return $foundHashtags;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postHashtag() {
        return $this->getHashtag();
    }
    
    public function getUser() {
        try {
            list($keyword) = ClopicValidator::instance()->inputs(array('keyword'));
            list($page) = ClopicValidator::instance()->inputOrDefault(array(
                'page' => 1
            ));
            
            Paginator::setCurrentPage($page);
            $foundUsername = User::whereRaw("username LIKE '$keyword%'")->simplePaginate(20)->all();
            foreach($foundUsername as $user) {
                $user->asset();
            }
            return $foundUsername;
        } catch (Exception $ex) {
            return Error::getInstance()->getError($ex->getCode(), $ex->getMessage(), $ex->getTraceAsString());
        }
    }
    
    public function postUser() {
        return $this->getUser();
    }
}
