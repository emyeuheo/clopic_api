<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
App::missing(function($exception)
{
    return Response::json(array('error'=>array('code' => 404, 'message' => 'Page not found')));
});

App::error(function(Exception $exception, $code)
{
    switch ($code) {
        case AUTH_INVALID_SESSION_KEY:
            return Response::json(Error::getInstance()->getError(AUTH_INVALID_SESSION_KEY)); 
            break;
        case AUTH_MISSING_SESSION_KEY:
            return Response::json(Error::getInstance()->getError(AUTH_MISSING_SESSION_KEY)); 
            break;
    }

});

Route::controller('/api/auth', 'ApiAuthController');
Route::controller('/api/user', 'ApiUserController');
Route::controller('/api/photo', 'ApiPhotoController');
Route::controller('/api/test', 'ApiTestController');
Route::controller('/api/action', 'ApiActionController');
Route::controller('/api/search', 'ApiSearchController');