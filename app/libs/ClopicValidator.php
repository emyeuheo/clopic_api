<?php

class ClopicValidator {

    private static $instance;

    public static function instance() {
        if (self::$instance == null) {
            self::$instance = new ClopicValidator();
        }
        return self::$instance;
    }

    public function inputs($fields = array()) {
        $returns = array();

        foreach ($fields as $field) {
            if (!Input::has($field)) {
                throw new Exception("Missing $field", 9999);
            }
            $returns[] = Input::get($field);
        }

        return $returns;
    }

    public function inputOrDefault($fields = array()) {
        $returns = array();

        foreach ($fields as $field=>$default) {
            if (!Input::has($field)) {
                $returns[] = $default;
            } else {
                $returns[] = Input::get($field);
            }
        }

        return $returns;
    }
    
    public function numeric($fields = array()) {
        $returns = array();

        foreach ($fields as $field) {
            if (!Input::has($field) || !is_numeric(Input::get($field))) {
                throw new Exception("$field is not a number or does not exist", 10000);
            }
            $returns[] = Input::get($field);
        }
        
        return $returns;
    }
    
    public function page() {
        $page = 1;
        if (Input::has('page') && is_numeric(Input::get('page')) && Input::get('page') > 0) {
            $page = Input::get('page');
        }
        
        return $page;
    }
    
    public function files($fields = array()) {
        $returns = array();

        foreach ($fields as $field) {
            if (!Input::hasFile($field) || !Input::file($field)->isValid()) {
                throw new Exception('PHOTO_MISSING_UPLOAD_FILE', PHOTO_MISSING_UPLOAD_FILE);
            }
            $returns[] = Input::file($field);
        }

        return $returns;
    }
    
    public function getXY() {
        $x = null;
        $y = null;
        if(Input::has('latitude') && is_numeric(Input::get('latitude'))) {
            $x = Input::get('latitude');
        }
        if(Input::has('longitude') && is_numeric(Input::get('longitude'))) {
            $y = Input::get('longitude');
        }
        return array($x, $y);
    }
    
    /**
     * Valid a string that match :
     * - "a-z","0-9",".","_"
     * - have maximum a character "."
     * 
     * @param String $username
     * @return boolean
     */
    public function isValidUsername($username) {
        return preg_match('/^[a-z0-9._]{3,15}$/', $username) && substr_count($username, '.') <= 1;
    }

    /**
     * Valid an email string
     * 
     * @param string $email
     * @return boolean
     */
    public function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($email) <= 255;
    }

    /**
     * Valid an password string
     * 
     * @param string $password
     * @return boolean
     */
    public function isValidPassword($password) {
        return strlen($password) <= 255;
    }

    /**
     * Valid a hashtag
     * 
     * @param string $hashtag
     * @return boolean
     */
    public function isValidHashtag($hashtag) {
        return preg_match('/^[a-zA-Z]+$/', $hashtag);
    }

    /**
     * Valid an image
     * 
     * @param Symfony\Component\HttpFoundation\File\UploadedFile $photo
     * @return boolean
     */
    public function isValidImage($photo) {
        $mimesAllowed = array(
            'image/gif',
            'image/jpeg',
            'image/pjpeg',
            'image/png'
        );
        return in_array($photo->getMimeType(), $mimesAllowed);
    }

    /**
     * Process username
     * 
     * @param string $username
     * @return boolean
     * @throws Exception
     */
    public function username($username) {
        if (!$this->isValidUsername($username)) {
            throw new Exception('REGISTER_INVALID_USERNAME', REGISTER_INVALID_USERNAME);
        }

        if (User::isExistedUsername($username)) {
            throw new Exception('REGISTER_EXISTED_USERNAME', REGISTER_EXISTED_USERNAME);
        }

        return true;
    }

    /**
     * Process email
     * 
     * @param string $email
     * @return boolean
     * @throws Exception
     */
    public function email($email) {
        if (!$this->isValidEmail($email)) {
            throw new Exception('REGISTER_INVALID_EMAIL', REGISTER_INVALID_EMAIL);
        }

        if (User::isExistedEmail($email)) {
            throw new Exception('REGISTER_EXISTED_EMAIL', REGISTER_EXISTED_EMAIL);
        }

        return true;
    }

    /**
     * Process password
     * 
     * @param string $password
     * @return boolean
     * @throws Exception
     */
    public function password($password) {
        if (!$this->isValidPassword($password)) {
            throw new Exception('REGISTER_TOO_LONG_PASSWORD', REGISTER_TOO_LONG_PASSWORD);
        }

        return true;
    }

    /**
     * Process image
     * 
     * @param Symfony\Component\HttpFoundation\File\UploadedFile $photo
     * @return boolean
     * @throws Exception
     */
    public function image($photo) {
        if (!$this->isValidImage($photo)) {
            throw new Exception('PHOTO_INVALID_FILE_TYPE', PHOTO_MISSING_UPLOAD_FILE);
        }

        return true;
    }

    /**
     * Process login
     * 
     * @param string $username
     * @param string $password
     * @return boolean
     * @throws Exception
     */
    public function login($username, $password) {
        if (empty($username) || empty($password)) {
            throw new Exception('LOGIN_INVALID_FORM', LOGIN_INVALID_FORM);
        }

        $userId = User::getIdByUP($username, $password);

        if (empty($userId)) {
            throw new Exception('LOGIN_WRONG_INFO', LOGIN_WRONG_INFO);
        }

        return $userId;
    }

    /**
     * Process hashtag
     * 
     * @param array $hashtags
     * @return boolean
     * @throws Exception
     */
    public function hashtags($hashtags = array()) {
        foreach ($hashtags as $tag) {
            if (!$this->isValidHashtag($tag)) {
                throw new Exception('HASHTAG_INVALID_HASHTAG', HASHTAG_INVALID_HASHTAG);
            }
        }

        return true;
    }

    public function isValidTimeStamp($timestamp) {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
}
