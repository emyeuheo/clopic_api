<?php

class IOUtils {

    public static function png2jpg($originalFile) {
        $image = imagecreatefrompng($originalFile);
        imagejpeg($image, $originalFile);
        imagedestroy($image);
    }
    
    public static function toJpeg($filename) {
        if(!file_exists($filename)) {
            return false;
        }
        
        if(preg_match('/png/', mime_content_type($filename))) {
            self::png2jpg($filename);
        }
        
        return true;
    }

}
