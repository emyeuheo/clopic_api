<?php

define('REGISTER_INVALID_FORM', 1);
define('REGISTER_INVALID_EMAIL', 2);
define('REGISTER_INVALID_USERNAME', 3);
define('REGISTER_EXISTED_EMAIL', 4);
define('REGISTER_EXISTED_USERNAME', 5);
define('REGISTER_TOO_LONG_PASSWORD', 6);
define('LOGIN_INVALID_FORM', 7);
define('LOGIN_WRONG_INFO', 8);
define('AUTH_MISSING_SESSION_KEY', 9);
define('AUTH_INVALID_SESSION_KEY', 10);
define('PHOTO_MISSING_UPLOAD_FILE', 11);
define('PHOTO_INVALID_FILE_TYPE', 12);
define('HASHTAG_INVALID_HASHTAG', 13);
define('FOLLOW_MISSING_INFO', 14);
define('LIKE_MISSING_INFO', 15);
define('MISSING_FIELD', 9999);

class Error {

    private static $instance;
    
    public $errorMsg = array(
        // Register
        1 => 'Some information is empty',
        2 => 'This is not an invalid email',
        3 => 'This is not an invalid username',
        4 => 'This email is registered with Clopic',
        5 => 'This username is registered with Clopic',
        6 => 'Your password is too long',
        
        // Login
        7 => 'Some information is empty',
        8 => 'Username or password is not correct',
        
        // Auth
        9 => 'Missing session key in request',
        10 => 'Invalid session key',
        
        // Photo
        11 => 'Missing image when upload',
        12 => 'File uploaded is not an image',
        
        // Hashtag
        13 => 'Hashtag is invalid',
        
        // Follow
        14 => 'Missing user id or username',
        
        // Like
        15 => 'Missing photo id',
        
        
        
        
        9999 => 'Missing some field',
        
        /// User DB error
        
        1003 => 'Password is incorrect',
        1002 => 'New passwords do not match'
    );

    public function getErrorMsg($errorId) {
        return $this->errorMsg[$errorId];
    }
    
    public function getError($errorId, $trueMsg = '', $trace = '') {
        if(!isset($this->errorMsg[$errorId])) {
            return array('error' => array(
                'code' => $errorId,
                'message' => 'System error, contact developer',
                'true_message' => $trueMsg,
                'trace' => $trace
            ));
        }
        return array('error' => array(
            'code' => $errorId,
            'message' => $this->errorMsg[$errorId],
            'true_message' => $trueMsg
        ));
    }

    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new Error();
        }
        return self::$instance;
    }
}
