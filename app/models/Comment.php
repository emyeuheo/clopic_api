<?php

// 1800

define('COMMENT_CANNOT_INSERT_DB', 1801);
define('COMMENT_ACTION_NOT_ALLOWED', 1802);

class Comment extends Eloquent {

    public $table = 'clopic_comment';
    
    public function asset() {
    }
    
    public static function doComment($userId, $photoId, $text) {
        $comment = new Comment();
        $comment->user_id = $userId;
        $comment->photo_id = $photoId;
        $comment->text = $text;
        
        if(!$comment->save()) {
            throw new Exception('COMMENT_CANNOT_INSERT_DB', COMMENT_CANNOT_INSERT_DB);
        }
        
        return $comment;
    }
    
    public static function deleteComment($userId, $commentId) {
        $comment = Comment::where('id', $commentId)->first();
        if(empty($comment)) {
            return array();
        }
        
        if($comment->user_id != $userId) {
            throw new Exception('COMMENT_ACTION_NOT_ALLOWED', COMMENT_ACTION_NOT_ALLOWED);
        }
        
        $comment->delete();
        
        return $comment;
    }
    
    public static function getCommentCount($photoId) {
        return Comment::where('photo_id', $photoId)->count();
    }
    
    public static function getPhotoComment($photoId, $page = 1) {
        Paginator::setCurrentPage($page);
        
        return DB::table('clopic_comment')
                ->leftJoin('clopic_users', 'clopic_comment.user_id', '=', 'clopic_users.id')
                ->select('clopic_comment.id', 'clopic_comment.text', 'clopic_comment.user_id', 'clopic_users.username', 'clopic_comment.created_at')
                ->where('clopic_comment.photo_id', $photoId)
                ->orderBy('clopic_comment.id', 'DESC')
                ->simplePaginate(10)
                ->all();
    }
}