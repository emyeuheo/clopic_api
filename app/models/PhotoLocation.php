<?php

define('PHOTOLOCATION_CANNOT_INSERT_DB', 1701);

class PhotoLocation extends Eloquent {
    
    const KILOMETER_PER_DEGREE = 111.045;
    
    public $timestamps = false;
    
    public $table = 'clopic_photo_location';

    public static function setLocationForPhoto($photoId, $x, $y, $address) {
        if(!$x || !$y) {
            return null;
        }
        
        $return = DB::insert("INSERT INTO clopic_photo_location (photo_id, coordinate, longitude, latitude, address) VALUES ($photoId, POINT($x, $y), $x, $y, '$address')");
        
        if(!$return) {
            throw new Exception('PHOTOLOCATION_CANNOT_INSERT_DB', PHOTOLOCATION_CANNOT_INSERT_DB);
        }
        
        return PhotoLocation::where('photo_id', $photoId)->first();
    }
    
    public static function getLocationByPhoto($photoId) {
        return PhotoLocation::where('photo_id', $photoId)->first();
    }
    
    public static function nearestByKm($photoId, $distance = 0, $page = 1) {
        if($distance <= 0) {
            return array();
        }
        $photoLocation = PhotoLocation::where('photo_id', $photoId)->first();
        
        if(empty($photoLocation)) {
            return array();
        }
        
        $deltaDegree = $distance / self::KILOMETER_PER_DEGREE;
        $longUpperBound = ((double)$photoLocation->longitude)+$deltaDegree;
        $longLowerBound = ((double)$photoLocation->longitude)-$deltaDegree;
        $latUpperBound = ((double)$photoLocation->latitude)+$deltaDegree;
        $latLowerBound = ((double)$photoLocation->latitude)-$deltaDegree;
        
        Paginator::setCurrentPage($page);
        $locations = PhotoLocation::whereRaw("(longitude >= $longLowerBound AND longitude <= $longUpperBound) "
                . "AND (latitude >= $latLowerBound AND latitude <= $latUpperBound) AND photo_id <> $photoId")
                ->simplePaginate(Photo::MAX_ITEM_A_PAGE)
                ->all();
        
        $photoIds = array();
        foreach($locations as $location) {
            $photoIds[] = $location->photo_id;
        }
        
        $photos = Photo::massAsset(Photo::whereRaw('id IN ('.implode(',', $photoIds).')')->get());
        
        return $photos;
    }
    
    public function unpack() {
        Utils::copyProperty($this, Utils::unpackXY($this->coordinate), array('longitude','latitude'));
        return $this;
    }
}