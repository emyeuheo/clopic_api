<?php
use Carbon\Carbon;

define('LSESSION_CAN_NOT_CREATE_SESSION', 1101);

class LoginSession extends Eloquent {

    public $timestamps = false;
    private static $current = null;
    public $table = 'clopic_login_session';

    public function __destruct() {
        if(!$this->id) {
            return;
        }
        $this->last_activity_time = Carbon::now()->toDateTimeString();
        LoginSession::where('id', $this->id)->update(array('last_activity_time' => $this->last_activity_time));
    }
    
    public static function getSession($sessionKey) {
        return LoginSession::where('session_key', $sessionKey)->first();
    }

    public static function isExistSession($sessionKey) {
        return LoginSession::where('session_key', $sessionKey)->count() > 0;
    }

    public static function newSession($user) {
        $newSession = new LoginSession();
        $newSession->session_key = self::generateSessionKey();
        $newSession->session_ip = Request::ip();
        $newSession->user_id = $user->id;
        $newSession->created_at = $date = Carbon::now()->toDateTimeString();
        $newSession->last_activity_time = $date;
        if (!$newSession->save()) {
            throw new Exception('LSESSION_CAN_NOT_CREATE_SESSION', LSESSION_CAN_NOT_CREATE_SESSION);
        }
        $user->asset();
        $newSession->user = $user;
        return $newSession;
    }

    public static function generateSessionKey() {
        do {
            $sessionKey = Utils::randomString(25);
        } while (self::isExistSession($sessionKey));

        return $sessionKey;
    }

    public static function current() {
        if (self::$current == null) {
            if (!Input::has('sesskey')) {
                throw new Exception('AUTH_MISSING_SESSION_KEY', AUTH_MISSING_SESSION_KEY);
            }

            $sessionKey = Input::get('sesskey');
            $session = self::getSession($sessionKey);

            if (empty($session)) {
                throw new Exception('AUTH_INVALID_SESSION_KEY', AUTH_INVALID_SESSION_KEY);
            }

            self::$current = $session;
        }
        
        return self::$current;
    }

}
