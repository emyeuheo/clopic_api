<?php

define('MAPDB_CANNOT_CREATE', 1401);

class MapHashPhoto extends Eloquent {

    public $timestamps = false;
    public $table = 'clopic_map_hash_photo';

    public static function newMap($hashId, $photoId) {
        $newMap = new MapHashPhoto();
        $newMap->hash_tag_id = $hashId;
        $newMap->photo_id = $photoId;

        if (!$newMap->save()) {
            throw new Exception('MAPDB_CANNOT_CREATE', MAPDB_CANNOT_CREATE);
        }

        return $newMap;
    }

    public static function newMaps($hashTags, $photoId) {
        $newMaps = array();
        foreach ($hashTags as $hashTag) {
            $hashId = $hashTag;
            if ($hashTag instanceof Hashtag) {
                $hashId = $hashTag->id;
            }
            
            if(($newMap = self::newMap($hashId, $photoId))) {
                $newMaps[] = $newMap;
            }
        }
        
        return $newMaps;
    }

}
