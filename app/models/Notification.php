<?php

define('NOTI_CANNOT_INSERT_DB', 1901);

use Carbon\Carbon;

class Notification extends Eloquent {
    public $table = 'clopic_notification';
    public $timestamps = false;
    
    public static function createNew($userId, $otherUserId, $action, $message, $photoId) {
//        $noti = Notification::where('user_id', $userId)
//                ->where('other_user_id', $otherUserId)
//                ->where('action', $action)
//                ->where('photo_id', $photoId)->first();
//        
//        if(!empty($noti)) {
//            return $noti;
//        }
        
        if($userId == $otherUserId) {
            return array();
        }
        
        $newNoti = new Notification();
        $newNoti->user_id = $userId;
        $newNoti->other_user_id = $otherUserId;
        $newNoti->action = $action;
        $newNoti->message = $message;
        $newNoti->photo_id = $photoId;
        $newNoti->created_at = Carbon::now()->toDateTimeString();
        
        if(!$newNoti->save()) {
            throw new Exception('NOTI_CANNOT_INSERT_DB', NOTI_CANNOT_INSERT_DB);
        }
        
        return $newNoti;
    }
    
    public static function deleteByInfo($userId, $otherUserId, $action, $photoId) {
        $noti = Notification::where('user_id', $userId)
                ->where('other_user_id', $otherUserId)
                ->where('action', $action)
                ->where('photo_id', $photoId)->first();
        
        if(!empty($noti)) {
            $noti->delete();
            return $noti;
        }
    }
    
    public static function getNotiFeed($userId) {
        $notis = Notification::where('user_id', $userId)->orderBy('id', 'DESC')->get();
        foreach($notis as $noti) {
            $photo = Photo::photoDetail($noti->photo_id);
            if(!empty($photo)) {
                $noti->photo_url = asset($photo->photo_path);
            }
        }
        
        return $notis;
    }
}
