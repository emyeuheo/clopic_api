<?php
use Carbon\Carbon;

define('PHOTO_CAN_NOT_CREATE_PHOTO', 1201);
define('PHOTO_CAN_NOT_CONVERT_TO_JPEG', 1202);

class Photo extends Eloquent {
    public $timestamps = false;
    
    public $table = 'clopic_photo';
    public $hidden = array('photo_path');
    
    const MAX_ITEM_A_PAGE = 12;
    
    public function asset() {
        $user = User::where('id', $this->user_id)->first();
        
        $this->username = $user->username;
        $this->url = asset($this->photo_path);
        $this->like_count = Like::getLikeCount($this->id);
        $this->collect_count = Photo::where('original_id', $this->id)->count();
        $this->comment_count = Comment::getCommentCount($this->id);
        $this->hashtag = Hashtag::getPhotoHashtag($this->id);
        $this->is_liked = Like::where('user_id', LoginSession::current()->user_id)->where('photo_id', $this->id)->count();
        if($this->user_id != LoginSession::current()->user_id) {
            $this->is_collected = Photo::where('original_id', $this->id)->where('user_id', LoginSession::current()->user_id)->count();
        }
        return $this->url;
    }
    
    public function location() {
        $location = PhotoLocation::getLocationByPhoto($this->id);
        if(empty($location)) {
            return null;
        }
        $location->unpack();
        Utils::copyProperty($this, $location, array('address','longitude','latitude'));
        return $this;
    }
    
    public function unpackCoordinate() {
        $tmp = $this->coordinate;
        unset($this->coordinate);
        if(!$tmp) {
            return $this;
        }
        Utils::copyProperty($this, Utils::unpackXY($tmp), array('latitude','longitude'));
        return $this;
    }
    
    public static function photoDetail($photoId) {
        $photo = Photo::where('id', $photoId)->first();
        
        if(empty($photo)) {
            return array();
        }
        
        $photo->asset();
        $photo->location();
        
        return $photo;
    }
    
    public static function newPhoto($photoPath, $userId, $caption, $originalId = 0) {
        $newPhoto = new Photo();
        
        $photoFileName = self::generatePhotoFileName();
        $path = '/data/'.$photoFileName;
        
        $fullPath = public_path().$path;
        
        copy($photoPath, $fullPath);
        
        if(!IOUtils::toJpeg($fullPath)) {
            throw new Exception('PHOTO_CAN_NOT_CONVERT_TO_JPEG', PHOTO_CAN_NOT_CONVERT_TO_JPEG);
        }
        
        $newPhoto->photo_path = $path;
        $newPhoto->user_id = $userId;
        $newPhoto->caption = $caption;
        $newPhoto->created_at = Carbon::now()->toDateTimeString();
        $newPhoto->original_id = $originalId;
        
        if(!$newPhoto->save()) {
            throw new Exception('PHOTO_CAN_NOT_CREATE_PHOTO', PHOTO_CAN_NOT_CREATE_PHOTO);
        }
        
        unlink($photoPath);
        
        return $newPhoto;
    }
    
    public static function generatePhotoFileName() {
        return Utils::randomString(30).".jpg";
    }
    
    public static function getPhoto($userId, $page = 1, $mustHasLocation = false, $limit = self::MAX_ITEM_A_PAGE) {
        Paginator::setCurrentPage($page);
        
        if(!is_array($userId)) {
            $ids = array($userId);
        } else {
            $ids = $userId;
        }
        
        $table = DB::table('clopic_photo');
        
        if($mustHasLocation != false) {
            $table = $table->join('clopic_photo_location', 'clopic_photo.id', '=', 'clopic_photo_location.photo_id');
        } else {
            $table = $table->leftJoin('clopic_photo_location', 'clopic_photo.id', '=', 'clopic_photo_location.photo_id');
        }
        
        $data = Photo::massAsset(
                $table
                ->select(
                        'clopic_photo.id', 
                        'clopic_photo.photo_path', 
                        'clopic_photo.caption', 
                        'clopic_photo.user_id',
                        'clopic_photo.created_at',
                        'clopic_photo_location.coordinate',
                        'clopic_photo_location.address',
                        'clopic_photo.original_id'
                )
                ->whereRaw('clopic_photo.user_id IN ('.implode(',' , $ids).')')
                ->orderBy('clopic_photo.id', 'DESC')
                ->simplePaginate($limit)
                ->all()
        );
        
        return $data;
    }
    
    public static function getPhotoByHashtag($hashtag, $page = 1, $userId = null, $mode = 'data') {        
        $hashtag = Hashtag::where('hash_tag', $hashtag)->first();
        if(empty($hashtag)) {
            if($mode == 'data') {
                return array();
            }
            if($mode == 'count') {
                return array('count' => 0);
            }
        }
        
        $hashtagId = $hashtag->id;
        $tmps = MapHashPhoto::where('hash_tag_id', $hashtagId)->get();
        
        $photoIds = array();
        foreach ($tmps as $map) {
            $photoIds[] = $map->photo_id;
        }
        
        $userIdQuery = ($userId!=null) ? "AND user_id = $userId" : '';
        
        $queryObj = DB::table('clopic_photo')
                ->leftJoin('clopic_photo_location', 'clopic_photo.id', '=', 'clopic_photo_location.photo_id')
                ->select(
                        'clopic_photo.id', 
                        'clopic_photo.photo_path', 
                        'clopic_photo.caption', 
                        'clopic_photo.user_id',
                        'clopic_photo.created_at',
                        'clopic_photo_location.coordinate',
                        'clopic_photo_location.address',
                        'clopic_photo.original_id'
                )
                ->whereRaw('clopic_photo.id IN ('.implode(',' , $photoIds).') '.$userIdQuery)
                ->orderBy('clopic_photo.id', 'DESC');
        
        if($mode == 'data') {
            Paginator::setCurrentPage($page);
            return Photo::massAsset($queryObj->simplePaginate(self::MAX_ITEM_A_PAGE)->all());
        }
        if($mode == 'count') {
            return array('count' => $queryObj->count());
        }
    }
    
    public static function massAsset($photos) {
        foreach($photos as &$photo) {
            if(!($photo instanceof Photo)) {
                $tmpPhoto = new Photo();
                Utils::copyProperty($tmpPhoto, $photo);
                $photo = $tmpPhoto;
            }
            $photo->asset();
            $photo->unpackCoordinate();
        }
        
        return $photos;
    }
    
    public static function theClock($userId, $startTime, $endTime, $currentLong, $currentLat, $distance, $page = 1) {        
        
        $deltaDegree = $distance / PhotoLocation::KILOMETER_PER_DEGREE;
        $longUpperBound = ((double)$currentLong)+$deltaDegree;
        $longLowerBound = ((double)$currentLong)-$deltaDegree;
        $latUpperBound = ((double)$currentLat)+$deltaDegree;
        $latLowerBound = ((double)$currentLat)-$deltaDegree;
        
        $userIds = Utils::objColToArray(UserRelationship::getFollowing($userId), 'other_user_id');
        $userIds[] = $userId;
        
        Paginator::setCurrentPage($page);
        
        $data = Photo::massAsset(
                DB::table('clopic_photo')->join('clopic_photo_location', 'clopic_photo.id', '=', 'clopic_photo_location.photo_id')
                ->select(
                        'clopic_photo.id', 
                        'clopic_photo.photo_path', 
                        'clopic_photo.caption', 
                        'clopic_photo.user_id',
                        'clopic_photo.created_at',
                        'clopic_photo_location.coordinate',
                        'clopic_photo_location.address',
                        'clopic_photo.original_id'
                )
                ->whereRaw('
                    (longitude >= '.$longLowerBound.' AND longitude <= '.$longUpperBound.') 
                    AND (latitude >= '.$latLowerBound.' AND latitude <= '.$latUpperBound.')'
                )
                ->orderBy('clopic_photo.id', 'DESC')
                ->simplePaginate(self::MAX_ITEM_A_PAGE)
                ->all()
                );
       
        if(empty($data)) {
            $data = Photo::whereRaw('user_id IN ('.implode(',', $userIds).') AND (created_at >= "'.$startTime.'" AND created_at <= "'.$endTime.'")')
                ->orderBy('clopic_photo.id', 'DESC')
                ->simplePaginate(self::MAX_ITEM_A_PAGE)
                ->all();
            
            foreach($data as $photo) {
                $photo->asset();
            }
        }
        
        return $data;
                
    }
    
    public static function collectPhoto($userId, $photoId) {
        $photo = Photo::where('id', $photoId)->first();
        
        if(empty($photo)) {
            return array();
        }
        
        $isCollect = Photo::where('original_id', $photoId)->where('user_id', $userId)->count();
        
        if($isCollect) {
            return array();
        }
        
        if($photo->original_id > 0) {
            $tmpPhotoId = $photo->original_id;
            $tmpPhoto = Photo::where('id', $tmpPhotoId)->first();
            if(!empty($tmpPhoto)) {
                $photoId = $tmpPhotoId;
                $photo = $tmpPhoto;
            }
        }
        
        $author = User::where('id', $photo->user_id)->first();
        
        if(empty($author)) {
            return array();
        }
        
        if($photo->user_id == $userId) {
            return array();
        }
        
        $newPhoto = new Photo();
        $newPhoto->photo_path = $photo->photo_path;
        $newPhoto->user_id = $userId;
        $newPhoto->caption = $photo->caption;
        $newPhoto->created_at = Carbon::now()->toDateTimeString();
        $newPhoto->original_id = $photo->id;
        $newPhoto->save();
        
        $hashIds = MapHashPhoto::where('photo_id', $photoId)->get();
        $hashIds = Utils::objColToArray($hashIds, 'hash_tag_id');
        MapHashPhoto::newMaps($hashIds, $newPhoto->id);

        $newHashTags = Hashtag::newHashTags(array($author->username));
        MapHashPhoto::newMaps($newHashTags, $newPhoto->id);
        
        $location = PhotoLocation::getLocationByPhoto($photoId);
        
        if(!empty($location)) {
            unset($location->id);
            $location->photo_id = $newPhoto->id;
            $location->save();
        }
        $newPhoto->asset();
        $newPhoto->location();
        
        return $newPhoto;
    }
    
    public static function deletePhoto($userId, $photoId) {
        $photo = Photo::where('user_id', $userId)->where('id', $photoId)->first();
        
        if(empty($photo)) {
            return array();
        }
        
        $photo->delete();
        
        return $photo;
    }
    
    public static function uncollectPhoto($userId, $originalId) {
        $photo = Photo::where('user_id', $userId)->where('original_id', $originalId)->first();
        
        if(empty($photo) || $originalId == 0) {
            return array();
        }
        
        $photo->delete();
        
        return $photo;
    }
}

Photo::saving(function($photo){
    unset($photo->url);
    unset($photo->like_count);
    unset($photo->comment_count);
    unset($photo->username);
    unset($photo->hashtag);
});

Photo::updating(function($photo){
    unset($photo->url);
    unset($photo->like_count);
    unset($photo->comment_count);
    unset($photo->username);
    unset($photo->hashtag);
});