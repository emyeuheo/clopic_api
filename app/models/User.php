<?php

use Carbon\Carbon;

define('USERDB_CAN_NOT_INSERT_NEW_USER', 1001);
define('USERDB_NEW_PASSWORDS_DO_NOT_MATCH', 1002);
define('USERDB_WRONG_PASSWORD', 1003);

class User extends Eloquent {

    protected $table = 'clopic_users';
    protected $hidden = array('password','avatar_path','created_at','updated_at');

    public function asset($getRelational = true) { // Show information to the world
        $this->avatar_url = (!empty($this->avatar_path)) ? asset($this->avatar_path) : '';
        $this->cover_url = (!empty($this->cover_path)) ? asset($this->cover_path) : '';
        
        if($getRelational) {
            $this->photo_count = Photo::where('user_id', $this->id)->count();
            try {
                $this->is_following = UserRelationship::where('other_user_id', $this->id)->where('user_id', LoginSession::current()->user_id)->where('relationship', UserRelationship::FOLLOWING)->count();
            } catch(Exception $ex) {
                
            }
            $this->follower_count = UserRelationship::where('other_user_id', $this->id)->where('relationship', UserRelationship::FOLLOWING)->count();
            $this->following_count = UserRelationship::where('user_id', $this->id)->where('relationship', UserRelationship::FOLLOWING)->count();
        }
        
        unset($this->avatar_path);
        unset($this->cover_path);
        
        return $this;
    }
    
    public static function isExistedUsername($username) {
        return User::where('username', $username)->count() > 0;
    }

    public static function isExistedEmail($email) {
        return User::where('email', $email)->count() > 0;
    }

    public static function newUser($username, $password, $email) {
        $newUser = new User();
        $newUser->username = $username;
        $newUser->password = md5($password);
        $newUser->email = $email;

        if (!$newUser->save()) {
            throw new Exception('USERDB_CAN_NOT_INSERT_NEW_USER', USERDB_CAN_NOT_INSERT_NEW_USER);
        }

        return $newUser;
    }

    public static function getIdByUP($username, $password) {
        return User::where('username', $username)->where('password', md5($password))->first();
    }

    public static function getInfo($userId) {
        $user = User::where('id', $userId)->first();
        if(empty($user)) {
            return array();
        }
        $user->asset();
        
        return $user;
    }

    public static function follow($userId, $otherId) {
        $userFollowed = User::where('id', $otherId)->first();
        if(empty($userFollowed)) {
            return array();
        }
        if(UserRelationship::isExist($userId, $otherId, UserRelationship::FOLLOWING)) {
            return $userFollowed;
        }
        
    }

    public static function unfollow() {
        
    }
    
    
    public static function uploadAvatar($userId, $photoPath, $type) {
        $photoFileName = Photo::generatePhotoFileName();
        $path = '/data/'.$photoFileName;
        
        $fullPath = public_path().$path;
        
        copy($photoPath, $fullPath);
        
        $user = User::where('id', $userId)->first();
        
        if($type=='avatar') {
            $user->avatar_path = $path;
        }
        if($type=='cover') {
            $user->cover_path = $path;
        }
        
        $user->save();
        $user->asset();
        return $user;
    }

    public static function updateInfo($userId, $data) {
        $user = User::where('id', $userId)->first();
        foreach($data as $key=>$value) {
            if($value==NULL) { 
                continue;
            }
            $user->$key = $value;
        }
        $user->save();
        return $user;
    }
    
    public static function changePassword($userId, $oldPassword, $newPassword, $newPassword2) {
        $user = User::where('id', $userId)->where('password', md5($oldPassword))->first();
        
        if(!$user) {
            throw new Exception('USERDB_WRONG_PASSWORD', USERDB_WRONG_PASSWORD);
        }
        
        if($newPassword !== $newPassword2) {
            throw new Exception('USERDB_NEW_PASSWORDS_DO_NOT_MATCH', USERDB_NEW_PASSWORDS_DO_NOT_MATCH);
        }
        
        $user->password = md5($newPassword);
        $user->save();
        
        return $user;
    }
    
}
