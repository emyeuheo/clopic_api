<?php

define('RELATIONSHIP_CANNOT_INSERT_DB', 1501);

class UserRelationship extends Eloquent {

    public $timestamps = false;
    public $table = 'clopic_user_relationship';

    const FOLLOWING = 'following';

    public static function isExist($userId, $otherId, $relationship) {
        return UserRelationship::where('user_id', $userId)->where('other_user_id', $otherId)->where('relationship', $relationship)->first();
    }

    public static function createNew($userId, $otherId, $relationship) {
        if ($userId == $otherId) {
            return array();
        }
        $relationshipObj = new UserRelationship();
        $relationshipObj->user_id = $userId;
        $relationshipObj->other_user_id = $otherId;
        $relationshipObj->relationship = $relationship;

        if (!$relationshipObj->save()) {
            throw new Exception('RELATIONSHIP_CANNOT_INSERT_DB', RELATIONSHIP_CANNOT_INSERT_DB);
        }

        return $relationshipObj;
    }

    public static function remove($userId, $otherId, $relationship) {
        $relationship = UserRelationship::where('user_id', $userId)->where('other_user_id', $otherId)->where('relationship', $relationship)->first();
        if (empty($relationship)) {
            return array();
        }
        $relationship->delete();
        $relationship->deleted = true;
        return $relationship;
    }

    public static function getFollowing($userId) {
        return UserRelationship::where('user_id', $userId)->where('relationship', UserRelationship::FOLLOWING)->get();
    }

    public static function getFollowerUsers($userId, $page = 1) {
        Paginator::setCurrentPage($page);
        
        $rels = UserRelationship::where('other_user_id', $userId)->where('relationship', UserRelationship::FOLLOWING)
                ->simplePaginate(Photo::MAX_ITEM_A_PAGE)
                ->all();
        
        if(empty($rels)) {
            return array();
        }
        
        $userIds = array();
        
        foreach($rels as $rel) {
            $userIds[] = $rel->user_id;
        }
                
        $users = User::whereRaw('id IN ('.implode(',', $userIds).')')->get();
        foreach($users as $user) {
            $user->asset(true);
        }
        
        return $users;
    }
    
    public static function getFollowingUsers($userId, $page = 1) {
        Paginator::setCurrentPage($page);
        
        $rels = UserRelationship::where('user_id', $userId)->where('relationship', UserRelationship::FOLLOWING)
                ->simplePaginate(Photo::MAX_ITEM_A_PAGE)
                ->all();
        
        if(empty($rels)) {
            return array();
        }
        
        $userIds = array();
        
        foreach($rels as $rel) {
            $userIds[] = $rel->other_user_id;
        }
                
        $users = User::whereRaw('id IN ('.implode(',', $userIds).')')->get();;
        foreach($users as $user) {
            $user->asset(true);
        }
        
        return $users;
    }

}
