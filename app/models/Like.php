<?php
use Carbon\Carbon;

define('LIKE_CANNOT_INSERT_DB', 1601);

class Like extends Eloquent {
    
    public $timestamps = false;
    
    public $table = 'clopic_like';
    
    public static function doLike($userId, $photoId) {
        $tmpLike = Like::where('user_id', $userId)->where('photo_id', $photoId)->first();
        if(!empty($tmpLike)) {
            return array();
        }
        $like = new Like();
        $like->user_id = $userId;
        $like->photo_id = $photoId;
        $like->created_at = Carbon::now()->toDateTimeString();
        
        if(!$like->save()) {
            throw new Exception('LIKE_CANNOT_INSERT_DB', LIKE_CANNOT_INSERT_DB);
        }
        
        return $like;
    }
    
    public static function doUnlike($userId, $photoId) {
        $like = Like::where('user_id', $userId)->where('photo_id', $photoId)->first();
        if(empty($like)) {
            return array();
        }
        $like->delete();
        
//        if(!$like->save()) {
//            throw new Exception('LIKE_CANNOT_INSERT_DB', LIKE_CANNOT_INSERT_DB);
//        }
//        
        return $like;
    }
    
    public static function getLikeCount($photoId) {
        return Like::where('photo_id', $photoId)->count();
    }
}

