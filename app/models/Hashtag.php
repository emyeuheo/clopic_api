<?php

use Carbon\Carbon;

define('HASHTAGDB_CANNOT_CREATE_HASHTAG', 1301);

class Hashtag extends Eloquent {

    public $timestamps = false;
    public $table = 'clopic_hash_tag';

    public static function newHashTag($hashtag) {
        $hashtagObj = Hashtag::where('hash_tag', $hashtag)->first();
        if (!empty($hashtagObj)) {
            return $hashtagObj;
        }

        $newHashtag = new Hashtag();
        $newHashtag->hash_tag = $hashtag;

        if (!$newHashtag->save()) {
            throw new Exception('HASHTAGDB_CANNOT_CREATE_HASHTAG', HASHTAGDB_CANNOT_CREATE_HASHTAG);
        }

        return $newHashtag;
    }

    public static function newHashTags($tags = array()) {
        $newHashtags = array();
        foreach ($tags as $hashtag) {
            $newHashtag = self::newHashTag($hashtag);
            if ($newHashtag) {
                $newHashtags[] = $newHashtag;
            }
        }
        return $newHashtags;
    }

    public static function getPhotoHashtag($photoId) {
        $hashIds = MapHashPhoto::where('photo_id', $photoId)->get();
        $hashIds = Utils::objColToArray($hashIds, 'hash_tag_id');
        if (count($hashIds)) {
            return Hashtag::whereRaw('id IN (' . implode(',', $hashIds) . ')')->get();
        } else {
            return array();
        }
    }
    
    public static function getUserHashtag($userId, $page = 1) {
        Paginator::setCurrentPage($page);
        $data = DB::table('clopic_photo')
                ->join('clopic_map_hash_photo', 'clopic_photo.id', '=', 'clopic_map_hash_photo.photo_id')
                ->join('clopic_hash_tag', 'clopic_map_hash_photo.hash_tag_id', '=', 'clopic_hash_tag.id')
                ->select(
                        'clopic_hash_tag.id', 
                        'clopic_hash_tag.hash_tag'
                )
                ->where('clopic_photo.user_id', $userId)
                ->groupBy('clopic_hash_tag.id')
                ->simplePaginate(Photo::MAX_ITEM_A_PAGE)
                ->all();
        
        return $data;
    }

}
