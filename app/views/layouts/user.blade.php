<html>
    <head>
        <meta charset='utf-8' />
        <title>@yield('siteTitle','Convert.io')</title>
        <link rel='stylesheet' href='<?=true_asset('/res/css/bootstrap.min.css')?>' />
        <!--<link rel='stylesheet' href='/res/css/bootstrap-theme.css' />-->
        <link rel='stylesheet' href='<?=true_asset('/res/css/cyfile.css')?>' />
        @yield('addonStyle')
        <script type='text/javascript' src='<?=true_asset('/res/js/jquery.js')?>'></script>
        <script type="text/javascript" src="<?=true_asset('/res/js/jquery.form.min.js')?>"></script>
        <script>cio = document;</script>
        @yield('addonScript')
        
    </head>
    <body>
        <div class='container'>
            @yield('header')
            @yield('mainContent')
            @yield('footer')
        </div>
    </body>
    <script type='text/javascript' src='<?=true_asset('/res/js/bootstrap.min.js')?>'></script>
    @yield('addonScriptAfterBody')
</html>