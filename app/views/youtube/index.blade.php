@extends('layouts.user')
@extends('layouts.header')
@extends('layouts.footer')

@section('addonScript')
@stop

@section('addonStyle')
<style type='text/css'>
    h1.pageTitle {
        color: #555;
        font-weight: bold;
    }
    .main {
        text-align: center;
        margin-top: 200px;
    }
    #inputYoutubeURL {
        max-width: 500px;
        width: 100%;
        font-size: 23px;
        /*padding: 23px 10px;*/
        height: 48px;
    }
    #downloadBtn {
        font-size : 24px;
        font-weight: bold;
    }
</style>
@stop

@section('mainContent')
<div class='main'>
    <div class='pageTitleWrapper'>
        <h1 class='pageTitle'>Download mp3 từ Youtube</h1>
        <noscript>Hãy bật tính năng Javascript của trình duyệt để sử dụng</noscript>
    </div>
    <div id='inputForm'>
        <div class="form-group">
            <form class="form-inline" role="form">
                <div class="input-group">
                    <label class="sr-only" for="exampleInputEmail2">Youtube URL</label>
                    <div class="input-group-addon">&lt;/&gt;</div>
                    <input type="text" class="form-control" id="inputYoutubeURL" placeholder="Paste link youtube vào đêi" value="" />
                </div>
                <button id='downloadBtn' class="btn btn-success">
                    <span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>
                    Download luôn
                </button>
            </form>
        </div>
        <div class='pleaseWaitWrapper'>
            <h3 id='pleaseWait'></h3>
        </div>
    </div>
</div>
@stop

@section('addonScriptAfterBody')
<script type='text/javascript'>
    cio.pleaseWait = function() {
        cio._waitDots = 1;
        cio._intervalWait = setInterval(function(){
            var str = 'Quẩy lên';
            for(var i=0;i<cio._waitDots;i++) {
                str += '.';
            }
            $('h3#pleaseWait').html(str);
            cio._waitDots++;
            if(cio._waitDots > 5) cio._waitDots = 1;
        }, 1000);
    }
    cio.stopWait = function() {
        clearInterval(cio._intervalWait);
        $('h3#pleaseWait').html('');
    }
    $('#downloadBtn').click(function(event) {
        event.preventDefault();
        $(this).attr('disabled', 'disabled');
        cio.pleaseWait();
        
        var url = $('#inputYoutubeURL').val();
        $.get('<?=action('YoutubeController@getSource')?>', {'url': url }, function(data) {
            var json = data || $.parseJSON(data);
            if (json.error) {
                alert('Có lỗi. Xin hãy thử lại');
            } else {
                window.location.href = json.url;
            }
            $('#downloadBtn').removeAttr('disabled');
            cio.stopWait();
        });
    });
</script>
@stop