<?php

function true_asset($path) {
    $documentRoot = $_SERVER['DOCUMENT_ROOT'];
    $real = realpath(public_path().$path);
    return asset(str_replace($documentRoot,"", $real));
}